                                        ; INIT
(package-initialize)

;; (add-to-list 'package-archives
;;              '("melpa-stable" . "https://stable.melpa.org/packages/") t)

(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/"))

(setq inhibit-startup-message t)
(setq make-backup-files nil)
(show-paren-mode 1)
(menu-bar-mode -1)
(setq scroll-step 1)
(setq-default indent-tabs-mode nil)
(global-auto-revert-mode 1)
(setq gc-cons-threshold 20000000) ; POTENTIALLY STUPID
(ac-config-default)
(add-hook 'before-save-hook 'delete-trailing-whitespace)
(load-theme 'whiteboard)

                                        ; KEYS
(global-set-key (kbd "C-x C-m") 'execute-extended-command)
(global-set-key (kbd "C-x m") 'execute-extended-command)
(global-set-key (kbd "C-x C-k") 'kill-region)
(global-set-key (kbd "C-x C-b") 'ibuffer)
(global-set-key (kbd "C-w") 'backward-kill-word)
(global-set-key (kbd "C-x C-j") 'fiplr-find-file)

                                        ; FUNCTIONS
(defun copy-from-osx ()
  (shell-command-to-string "pbpaste"))

(defun paste-to-osx (text &optional push)
  (let ((process-connection-type nil))
    (let ((proc (start-process "pbcopy" "*Messages*" "pbcopy")))
      (process-send-string proc text)
      (process-send-eof proc))))

(setq interprogram-cut-function 'paste-to-osx)
(setq interprogram-paste-function 'copy-from-osx)

                                        ; RUBY
(add-hook 'ruby-mode-hook
          (lambda ()
            (ruby-electric-mode)
            (subword-mode)))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages (quote (fiplr ruby-electric ruby-tools auto-complete))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
